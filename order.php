<?php 
// this source was not openSource contact trist8n.56@hotmail.fr for more information
if($_SERVER['HTTP_REFERER']==="https://www.velvetrosevodka.com/newSite/newSite/shop"){

    function validerNumero($telATester) {
        //Retourne le numéro s'il est valide, sinon false.
        return preg_match('`^0[1-9]([-. ]?[0-9]{2}){4}$`', $telATester) ? $telATester : false;
    }
    
    require 'vendor/autoload.php';
    
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    
    \Stripe\Stripe::setApiKey("sk_test_fmwQexuOnQUgv6m41tJGGZhF");
    
    // Token is created using Checkout or Elements!
    // Get the payment token ID submitted by the form:
        $token = $_POST['tokenid'];
         
        // sanitize and validate information send from this shop page
        $amount = filter_var($_POST['amount'],FILTER_VALIDATE_INT);
        $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
        $identite = filter_var($_POST['identite'],FILTER_SANITIZE_STRING);
        $adresse = filter_var($_POST['adresse'],FILTER_SANITIZE_STRING);
        $complementadresse = filter_var($_POST['complementAdresse'],FILTER_SANITIZE_STRING);
        $postal = filter_var($_POST['postal'],FILTER_VALIDATE_INT);
        $ville = filter_var($_POST['ville'],FILTER_SANITIZE_STRING);
        $quantity = filter_var($_POST['quantite'],FILTER_VALIDATE_INT);
        $phone = filter_var($_POST['phone'], FILTER_CALLBACK, array('options' => 'validerNumero'));
    
        $booltestVar=true;
    
        $testVar =[$amount,$email,$identite,$adresse,$postal,$ville,$quantity,$phone];

     // if any information was wrong return 3 for information message
        for($i=0;$i<=7;$i++){
            if($testVar[$i]===false){
                $booltestVar=false;
                echo('3');
                break;
            }else if($testVar[$i]===null){
                $booltestVar=false;
                echo('3');
                break;
            }else{
                $booltestVar=true;
            }
        }
    
        // if token send from payement popup and information was fine  
    if($token!=null && $booltestVar==true){
    
        // if amount was inferior at 130euro (4bottle) add shipping cost
    if($amount<=13000){
        $order = \Stripe\Order::create([
            'currency' => 'eur',
            'email' => $email,
            'items' => [
                [
                    'type' => 'sku',
                    'parent' => 'sku_EQsT0rcxAp97FG',
                    'quantity' => $quantity,
                ],
                 [
                    'type' => 'sku',
                    'parent' => 'sku_ETOlZ9f4hI5vpQ',
                    'quantity' => 1,
                ],
            ],
            "metadata"=> [
                'phone'=>$phone
            ],
            'shipping' =>  [
                'name' => $identite,
                'address' => [
                    'line1' => $adresse,
                    'line2'=>$complementadresse,
                    'city' => $ville,
                    'postal_code' => $postal,
                    'country' => 'FR',
                ],
            ],
        ]);
        // if order contain more than 3bottle the shipping cost was not add because it's free
    }else{
       $order = \Stripe\Order::create([
        'currency' => 'eur',
        'email' => $email,
        'items' => [
            [
                'type' => 'sku',
                'parent' => 'sku_EQsT0rcxAp97FG',
                'quantity' => $quantity,
            ],
        ],
        "metadata"=> [
            'phone'=>$phone
        ],
        'shipping' =>  [
            'name' => $identite,
            'address' => [
                'line1' => $adresse,
                'line2'=>$complementadresse,
                'city' => $ville,
                'postal_code' => $postal,
                'country' => 'FR',
            ],
        ],
    ]);  
    }
    
    // after the order was creat and send to stripe server we find it for confirm she's create
    $orderConfirm = \Stripe\Order::retrieve($order['id']);
    
    
    if($orderConfirm!=null){

    // if stripe serveur return an order, create bill

      define( 'MAIL_TO', /* >>>>> */'Trist8n.56@hotmail.fr'/*'business@velvetrosevodka.com'*//* <<<<< */ );  //ajouter votre courriel
      $object=mb_encode_mimeheader("Récapitulatif nouvelle Commande");
      $facture = "Votre commande est en cour de traitement, elle seras envoyée dans les plus bref délais, conservez précieusement cet e-mail. <br/> Ce document fait office de facture.";
      $produit = " bouteille 70cl Velvet Rose Vodka";
      $info ="<br/><br/>n°commande : ".$orderConfirm["id"]."<br/><br/>Produit :".$produit."<br/><br/>Quantité : ".$quantity."<br/><br/>Prix : ".$quantity*40;
      $fraisPort="<br/><br/>frais de port : 10.00€";
      $freeFraisPort = "<br/><br/>frais de port : 0.00€";
      $total = "<br/><br/>Total : ".($amount/100+10);
      $totalFree = "<br/><br/>Total : ".$amount/100;
      
      if($amount<=13000){
        $facture.=$info.".00€";
        $facture.=$fraisPort;
        $facture.=$total.".00€";
      }else{
        $facture.=$info.".00€";
        $facture.=$freeFraisPort;
        $facture.=$totalFree.".00€";
      }

      //getting CGV from serveur and adding to the mail
      $file_name = "CGV.pdf";
      $path = $_SERVER['DOCUMENT_ROOT']."/newSite/newSite/";
      $typepiecejointe = filetype($path.$file_name);
      $data = chunk_split( base64_encode(file_get_contents($path.$file_name)) );
      //Génération du séparateur
      $boundary = md5(uniqid(time()));
      $entete = "From: Velvet Rose Vodka \n";
      $entete .= "Reply-to: ".MAIL_TO." \n";
      $entete .= "X-Priority: 1 \n";
      $entete .= "MIME-Version: 1.0 \n";
      $entete .= "Content-Type: multipart/mixed; boundary=\"$boundary\" \n";
      $entete .= " \n";
      $message  = "--$boundary \n";
      $message .= "Content-Type: text/html; charset=\"UTF8\" \n";
      $message .= "Content-Transfer-Encoding:32bit \n";
      $message .= "\n";
      $message .= $facture;
      $message .= "\n";
      $message .= "--$boundary \n";
      $message .= "Content-Type: $typepiecejointe; name=\"$file_name\" \n";
      $message .= "Content-Transfer-Encoding: base64 \n";
      $message .= "Content-Disposition: attachment; filename=\"$file_name\" \n";
      $message .= "\n";
      $message .= $data."\n";
      $message .= "\n";
      $message .= "--".$boundary."--";

      // if order was pay send mail 
      try{
            $orderConfirm->pay(['source' => $token]);

            mail( MAIL_TO, $object, "Nouvelle Commande reçu\n\nQuantité de produit : ".$quantity
            ."\n\nn°commande : ".$orderConfirm["id"], "From: <noreply@velvetrosevodka.com>" );
    
            mail( $email, $object, $message, $entete);
    
        // response for information message
            echo '1'; 
        }catch(\Stripe\Error\Card $e){
            // response for information message
            echo "0";
            // canceled order
            \Stripe\Order::update(
                $orderConfirm['id'],
                [
                  "status"=> "canceled"  
                ]
            );
            }
     
    }else{
        // response for information message
        echo'2';
    }
    }
}else{
    exit();
}
  ?>