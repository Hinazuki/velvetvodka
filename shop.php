<?php 
// this source was not openSource contact trist8n.56@hotmail.fr for more information
include 'auth.php';
    ?>
    <!DOCTYPE html>  
    <html xml:lang="fr">  
        <head>  
            <title>Velvet Rose Vodka</title>
        <meta name="author" content="France's new and innovative luxury vodka" />
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/styleCommande.css">
        <link rel="stylesheet" type="text/css" href="CSS/bootstrap.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="img/favicon1.png">
        </head>  
        <body class="backgrounds">  
        <div class="hautDePage">
            <div class="logo">
                <a href="index">
                    <img src="img/darklogo.png">
                </a>
            </div>
        </div> 
        <div class="container formu" id="cont">
        <div id="appen">
        <h3 class="textCentrer">Commande</h3>
        <p id="welcome"em class="textCentrer">Tous les champs sont obligatoires</p>
        </div>
                <form class="grouper" id="formCommande">
                <div>
                    <div class="form-group taille">
                        <label>Nom</label>
                        <input id="Nom" name="nom" placeholder="Nom" class="form-control">
                    </div>
                    <div class="form-group taille">
                        <label>Prénom</label>
                        <input id="Prenom" placeholder="Prénom" class="form-control">
                    </div>
                    <div class="form-group taille">
                        <label>Numéro de Téléphone</label>
                        <div style="display:flex;margin-top:2%">
                          <p style="margin-top:2%">+33</p>
                          <input id="phone" type="tel" placeholder="Numéro" class="form-control">
                        </div>
                        
                    </div>
                </div>
                  <div>
                    <div class="form-group taille">
                      <label>Adresse</label>
                      <input id="adresse" placeholder="Adresse" class="form-control">
                    </div> 
                    <div class="form-group taille">
                      <label>Complément adresse</label>
                      <input id="complementAdresse" placeholder="Complément Adresse" class="form-control">
                    </div> 
                    </div> 
                    <div class="">
                    <div class="form-group taille">
                      <label>Code postal</label>
                      <input id="postal" placeholder="Code Postal" class="form-control">
                    </div>
                    <div class="form-group taille">
                      <label>Ville</label>
                      <input id="ville" placeholder="Ville" class="form-control">
                    </div>
                    </div>
                </form>
                <h2> 40€ l'unité </h2>
                <h5>Une vodka parisienne, exclusivement parisienne. Livraisons uniquement en Ile de France.</h5>
                <p>Les frais de port sont de 10.00€ par commande et gratuit à partir de 4 bouteilles achetées</p>
                <div class="panneauItem">
                  <div>
                      <img src="img/final.jpg" style="max-height:100px; border-radius:10px">
                      <label>Bouteille Velvet Rose Vodka 70cl</label>
                  </div>
                      <input id="quantite" type="number" value="1" style="border-radius:10px">
                </div>
                      <button id="customButton" class="btn btn-dark" style="margin-top:2vh;margin-bottom:2vh;">Payer</button>
        </div>    
        </body>  

    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script>
    var handler = StripeCheckout.configure({
      key: 'pk_test_JVYGW1mdPTk5wT2IUwxmJqKR',
      image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
      locale: 'auto',
      token: function(token) {

        //getting value of input from the form
        var montant = document.getElementById('quantite').value * 4000;
        var ville = document.getElementById('ville').value;
        var numeroTel = "0"+document.getElementById('phone').value;
        var Adresse = document.getElementById('adresse').value;
        var complementAdresse = document.getElementById('complementAdresse').value;
        var postal =document.getElementById('postal').value;
        var identite = document.getElementById('Nom').value+" "+document.getElementById('Prenom').value;
        var quantity =montant/4000;

        // create form data to send
        var data = new FormData();
        var req = new XMLHttpRequest();

        // add key and value in the formData
        data.append("amount",montant);
        data.append("currency","eur");
        data.append("tokenid",token.id);
        data.append("email",token.email);
        data.append("adresse",Adresse);
        data.append("phone",numeroTel);
        data.append("complementAdresse",complementAdresse);
        data.append("postal",postal);
        data.append("ville",ville);
        data.append("identite",identite);
        data.append("quantite",quantity);

        //send formData
        req.open('POST','order', true);
        req.withCredentials = true;
        req.send(data);

        // notify user to wait until script php wa finish
        var wait = document.createElement('p');
        wait.id='wait';
        var textWait = document.createTextNode("Envoi en cour, veuillez patienter...");
        wait.appendChild(textWait);
        document.getElementById('appen').appendChild(wait);
        req.addEventListener("load",function(){

          //here we see what php script return and write message whit it
           if(req.response==1){

            document.getElementById('appen').removeChild(wait);
            var confirm = document.createElement('p');
            confirm.id='confirm';
            var textConfirm = document.createTextNode("commande confirmé un e-mail de confirmation vous a été transmi (vérifiez vos courriers indésirable)");
            confirm.appendChild(textConfirm);
            confirm.classList.add("confirmCommande");
            document.getElementById('appen').appendChild(confirm);

          }else if(req.response==0){

            document.getElementById('appen').removeChild(wait);
            var confirm = document.createElement('p');
            confirm.id='confirm';
            var textConfirm = document.createTextNode("Payement refusé Veuillez recommencer");
            confirm.appendChild(textConfirm);
            confirm.classList.add("payementRefuser");
            document.getElementById('appen').appendChild(confirm);

          }else if(req.response==2){

            document.getElementById('appen').removeChild(wait);
            var confirm = document.createElement('p');
            confirm.id='confirm';
            var textConfirm = document.createTextNode("Un problème est survenu veuillez réessayer ultérieurement");
            confirm.appendChild(textConfirm);
            confirm.classList.add("payementRefuser");
            document.getElementById('appen').appendChild(confirm);

          }else if(req.response==3){

            document.getElementById('appen').removeChild(wait);
            var confirm = document.createElement('p');
            confirm.id='confirm';
            var textConfirm = document.createTextNode("Une de vos données n'a pus être validé, Veuillez recommencer");
            confirm.appendChild(textConfirm);
            confirm.classList.add("payementRefuser");
            document.getElementById('appen').appendChild(confirm);
            }
        });
      }
    });
// discrib action of payement button
    document.getElementById('customButton').addEventListener('click', function(e) {

      var quantiteTest = document.getElementById('quantite');
      var nomTest = document.getElementById('Nom');
      var prenomTest = document.getElementById('Prenom');
      var villeTest = document.getElementById('ville');
      var postalTest = document.getElementById('postal');
      var adresseTest = document.getElementById('adresse');
      var telTest = document.getElementById('phone');
      var boolTel = true;
      var boolPostal = true;
      var reg_telephone_portable = '^[6-7][0-9]{8}$';

      var tabInput =[nomTest,prenomTest,villeTest,postalTest,adresseTest];
      
      // verify phone number
        if( telTest.value.match(reg_telephone_portable) ){
          boolTel =true;
        } else {
          boolTel = false;
        }

      // verify postalCode from user information 
        if(postal.value.substr(0,2)==75||postal.value.substr(0,2)==92||postal.value.substr(0,2)==77
        ||postal.value.substr(0,2)==93||postal.value.substr(0,2)==78||postal.value.substr(0,2)==94||
        postal.value.substr(0,2)==91||postal.value.substr(0,2)==95){
          boolPostal = true;
        }else{
          boolPostal =false;
        }

        //verify if user not let input empty
      if(quantiteTest.value<1 || quantiteTest.value.includes("e") || nomTest.value=="" || prenomTest.value=="" 
      || villeTest.value=="" || postalTest.value == ""|| postalTest.value.length<5|| postalTest.value.length>5
       || adresseTest.value==""|| boolTel == false||boolPostal==false){
         
         // 212=>227 add CSS class to notify user, any input was empty
        for (var i=0;i<tabInput.length;i++){
          if(tabInput[i].value==""){
            tabInput[i].classList.add("wrong");
          }else{
            tabInput[i].classList.remove("wrong");
          }
        }

        if(quantiteTest.value<1||quantiteTest.value.includes("e")){
          quantiteTest.classList.add("wrong");
        }

        if(boolTel==false){
          telTest.classList.add("wrong");
        }
        // 229=>235 notify user postal code was not correct for shipping
        if(boolPostal==false){
          alert("Nous ne livrons que dans le secteur d'île de france veuillez nous excuser");
        }

        if(postalTest.value.length<5 || postalTest.value.length>5){
          alert("Veuillez entrer un code postal correct");
        }
      }else{
        // setting payement popup with some information from the form
      var quantiteFree = document.getElementById('quantite').value;
      var montant = document.getElementById('quantite').value * 4000;
      var pseudo = document.getElementById('Nom').value+document.getElementById('Prenom').value;
      if(quantiteFree<=3){
        montant+=1000;
      }

      // Open Checkout with further options:
      handler.open({
        name: pseudo,
        description: 'bouteille 70cl x'+document.getElementById('quantite').value,
        currency: 'eur',
        amount: montant
        
      });
      e.preventDefault();
      }

    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
      handler.close();
    });
    </script>
        </html>
