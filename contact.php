
<?php  
// this source was not openSource contact trist8n.56@hotmail.fr for more information
include 'auth.php';
    define( 'MAIL_TO', /* >>>>> */'business@velvetrosevodka.com'/* <<<<< */ );  //ajouter votre courriel  
    define( 'MAIL_FROM', 'MonAdresse@hotmail.fr' ); // valeur par défaut  
    define( 'MAIL_OBJECT', 'objet du message' ); // valeur par défaut  
    define( 'MAIL_MESSAGE', 'votre message' ); // valeur par défaut  
    $mailSent = false; // drapeau qui aiguille l'affichage du formulaire OU du récapitulatif  
    $errors = array(); // tableau des erreurs de saisie  
      
    if( filter_has_var( INPUT_POST, 'send' ) ) // le formulaire a été soumis avec le bouton [Envoyer]  
    {  
        $from = filter_input( INPUT_POST, 'from', FILTER_VALIDATE_EMAIL );  
        if( $from === NULL || $from === MAIL_FROM ) // si le courriel fourni est vide OU égale à la valeur par défaut  
        {  
            $errors[] = 'Vous devez renseigner votre adresse de courrier électronique.';  
        }  
        elseif( $from === false ) // si le courriel fourni n'est pas valide  
        {  
            $errors[] = 'L\'adresse de courrier électronique n\'est pas valide.';  
            $from = filter_input( INPUT_POST, 'from', FILTER_SANITIZE_EMAIL );  
        }  

        $object = filter_input( INPUT_POST, 'object', FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_HIGH | FILTER_FLAG_ENCODE_LOW );  
        if( $object === NULL OR $object === false OR empty( $object ) OR $object === MAIL_OBJECT ) // si l'objet fourni est vide, invalide ou égale à la valeur par défaut  
        {  
            $errors[] = 'Vous devez renseigner l\'objet.';  
        }  

 /* pas besoin de nettoyer le message.   
 / [http://www.phpsecure.info/v2/article/MailHeadersInject.php]  
 / Logiquement, les parties message, To: et Subject: pourraient servir aussi à injecter quelque chose,  mais la fonction mail()  
 / filtre bien les deux dernières, et la première est le message, et à partir du moment où on a sauté une ligne dans l'envoi du mail,  
 / c'est considéré comme du texte; le message ne saurait donc rester qu'un message.*/  
        $message = filter_input( INPUT_POST, 'message', FILTER_UNSAFE_RAW );  
        if( $message === NULL OR $message === false OR empty( $message ) OR $message === MAIL_MESSAGE ) // si le message fourni est vide ou égale à la valeur par défaut  
        {  
            $errors[] = 'Vous devez écrire un message.';  
        }
/* Create the mail object using the Mail::factory method */
/* Ok send mail */
        if( count( $errors ) === 0 ) // si il n'y a pas d'erreurs  
        {  
            if( mail( MAIL_TO, $object, $message."\n\nE-mail from : ".$from, "noreply@velvetrosevodka.com" ) ) // tentative d'envoi du message  
            {  
                $mailSent = true;  
            }  
            else // échec de l'envoi  
            {  
                $errors[] = 'Votre message n\'à pas été envoyé.';  
            }  
        } 
    }  
    else // le formulaire est affiché pour la première fois, avec les valeurs par défaut  
    {  
        $from = MAIL_FROM;  
        $object = MAIL_OBJECT;  
        $message = MAIL_MESSAGE;  
    }  
?>  
<!DOCTYPE html>  
<html xml:lang="fr">  
    <head>  
        <title>Velvet Rose Vodka</title>
    <meta name="author" content="France's new and innovative luxury vodka" />
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="CSS/styleContact.css">
    <link rel="stylesheet" type="text/css" href="CSS/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/favicon1.png">
    </head>  
    <body class="backgrounds">  
    <div class="hautDePage">
        <div class="logo">
            <a href="index">
                <img src="img/darklogo.png">
            </a>
        </div>
    </div> 
    <div class="container formu" id="cont">
            <h3 class="textCentrer">Contact par Email</h3>
<?php  
    if( $mailSent === true ) // si le message a bien été envoyé, on affiche le récapitulatif  
    {  
?>  
        <p id="success">Votre message à bien été envoyé.</p>  
        <p><strong>Courriel pour la réponse :</strong><br /><?php echo( $from ); ?></p>  
        <p><strong>Objet :</strong><br /><?php echo( $object ); ?></p>  
        <p><strong>Message :</strong><br /><?php echo( nl2br( htmlspecialchars( $message ) ) ); ?></p>  
<?php  
    }  
    else // le formulaire est affiché pour la première fois ou le formulaire a été soumis mais contenait des erreurs  
    {  
        if( count( $errors ) !== 0 )  
        {  
            echo( "\t\t<ul>\n" );  
            foreach( $errors as $error )  
            {  
                echo( "\t\t\t<li>$error</li>\n" );  
            }  
            echo( "\t\t</ul>\n" );  
        }  
        else  
        {  
            echo( "\t\t<p id=\"welcome\"em class=\"textCentrer\">Tous les champs sont obligatoires</p>\n" );  
        }  
?>  
 
        <form id='contact' method="post" action="<?php echo( $_SERVER['REQUEST_URI'] ); ?>">
                <div class="form-group objet">
                    <label>Objet</label>
                    <input class="form-control" name="object" placeholder="Objet" id="object">
                </div>
                <div class="form-group email">
                        <label>Adresse E-Mail</label>
                        <input id="from" name="from" class="form-control" placeholder="E-Mail">
                    </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea name="message" class="form-control" id="message" rows="4" placeholder="Message"></textarea>
                </div>
                <button type="submit" class="btn btn-dark" name="send">Envoyer</button>
            </form>
    </div>    
<?php  
    }  
?>  
    </body>  
</html>