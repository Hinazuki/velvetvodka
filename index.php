<?php 
// this source was not openSource contact trist8n.56@hotmail.fr for more information
//page Accueil
include 'auth.php';
    ?><html>

    <head>
        <title>Velvet Rose Vodka</title>
        <meta name="author" content="France's new and innovative luxury vodka" />
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/AccueilStyle.css">
        <link rel="stylesheet" type="text/css" href="CSS/bootstrap.css">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <link rel="icon" href="img/favicon1.png">
    </head>
    
    <body class="general">

      <div class="bandeau" id="hautdepage">
      
            <div class="divMenu">
                <a href="index">
                    <img src="img/darklogo.png">
                </a>
            </div>
            <div class="menu">
                <ul>
                    <li class="fontTel">
                        <a href="#descriptif" class="itemMenu">Original Edition</a>
                            <p class="defilement">Découvrez nos Produits</p> 
                    </li>
                    <li class="fontTel">
                        <a href="#award" class="itemMenu">Awards</a>
                            <p class="defilement">Découvrez nos récompenses</p> 
                    </li>
                    <li class="fontTel">
                        <a href="shop" class="itemMenu">Acheter</a>
                            <p class="defilement">Cliquez pour acheter</p> 
                    </li>
                    <li class="fontTel">
                        <a href="contact" class="itemMenu">Contact</a>
                            <p class="defilement">Envie de nous contacter? C'est par ici</p> 
                    </li>
                </ul>
            </div>
    
        </div>
        <img src="img/Capture.PNG" class="centrer dimenssionRose" style="max-width: 100%;">
    
       <div class="colunm" id="descriptif">
            <div class="info">
                <h4>Bouteille: sérigraphie en argent</h4>
                <h4>Volume Bouteille: 70cl</h4>
                <h4>Alcool: 40%</h4>
            </div>
            <div class="description">
                <ul>
                    <h3>Du 100% français:</h3>
                    <li>
                        <h5>100% Blé tendre français</h5>
                    </li>
                    <li>
                        <h5>Distillée dans la région du Cognac</h5>
                    </li>
                </ul>
                <ul>
                    <h3>Une Vodka de Qualité:</h3>
                    <li>
                        <h5>Une filtration sur plaque de charbon actif pour plus de rondeur et de finesse</h5>
                    </li>
                    <li>
                        <h5>Aucun additif</h5>
                    </li>
                    <li>
                        <h5>Distillée 6 fois</h5>
                    </li>
                </ul>
                <ul>
                    <h3>Un goût particulier:</h3>
                    <li>
                        <h5>Une douceur et rondeur en bouche inégalée</h5>
                    </li>
                    <li>
                        <h5>Un nez frais et doux</h5>
                    </li>
                    <li>
                        <h5>Une texture soyeuse</h5>
                    </li>
                    <li>
                        <h5>Un long final chaleureux</h5>
                    </li>
                </ul>
            </div>
            <a href="#hautdepage">
                <img src="img/hautdepage.png" class="zoomEffectleft">
            </a>
        </div>
        <img src="img/Capture.PNG" class="centrer dimenssionRose" style="max-width: 100%;">
    
        <div class="award" id="award">
    
            <div class="alignement">
                <p>Awards</p>
                <img src="img/iwscsilver-2018.png" class="tailleMedaille">
                <p class="centertextaward">Après seulement 5 mois d'existence, 
                    l'Original Édition de Velvet Rose se distingue comme l'une des meilleures vodka françaises, 
                    gagnant la médaille d'argent au International Wine and Spirits Competition 2018 de Londres</p>
            </div>  
    
            <a href="#hautdepage">
                <img src="img/hautdepage.png" class="zoomEffectleft">
            </a>
        </div>
        <img src="img/drapeau.png" class="centrer" style="margin-top: 5%; margin-bottom: 3%; max-width: 100%">
    <div class="degrader">
            <div class="footer">
            <div class="artiFooter">
                <p>A propos de nous</p>
                <p>Velvet Rose est la vision de deux frères qui, éternellement insatisfaits par les vodkas sur le marché, décidèrent
                    qu'ils pouvaient faire mieux. Original Edition, résultat d'une recherche et de tests approfondis, est enfin
                    prête pour ambiancer vos jours et nuits.</p>
            </div>
            <div class="artiFooter">
                <p>SUIVEZ NOUS</p>
                <div style="display: flex; justify-content: center">
                    <img src="img/insta.jpg" class="insta">
                    <p>@VelvetRoseVodka</p>
                </div>
            </div>
        </div>
        <div>
            <p class="moderation">A consommer avec modération</p>
        </div>
        <div>
            <p class="rose">©Velvet Rose</p>
        </div>
    </div>
    
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var aLiens = document.querySelectorAll('a[href*="#"]');
                for (var i = 0, len = aLiens.length; i < len; i++) {
                    aLiens[i].onclick = function () {
                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                            var target = this.getAttribute("href").slice(1);
                        
                            if (target.length) {
                                scrollTo(document.getElementById(target).offsetTop-60, 1500);
                                return false;
                            }
                        }
                    };
                }
            });
            function scrollTo(element, duration) {
                var e = document.documentElement;
                if (e.scrollTop === 0) {
                    var t = e.scrollTop;
                    ++e.scrollTop;
                    e = t + 1 === e.scrollTop-- ? e : document.body;
                }
                scrollToC(e, e.scrollTop, element, duration);
            }
    
            function scrollToC(element, from, to, duration) {
                if (duration < 0) return;
                if (typeof from === "object") from = from.offsetTop;
                if (typeof to === "object") to = to.offsetTop;
                scrollToX(element, from, to, 0, 1 / duration, 20, easeOutCuaic);
            }
    
            function scrollToX(element, x1, x2, t, v, step, operacion) {
                if (t < 0 || t > 1 || v <= 0) return;
                element.scrollTop = x1 - (x1 - x2) * operacion(t);
                t += v * step;
                requestAnimationFrame(function () {
                    scrollToX(element, x1, x2, t, v, step, operacion);
                });
            }
    
            function easeOutCuaic(t) {
                t--;
                return t * t * t + 1;
            }
        </script>
    </body>
    
    </html>